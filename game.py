import random
import time

print ("\n\nHello and Welcome to Nishikesh's Tic Tac Toe game, coded in Python! \n\n")
print(" 1 | 2 | 3 ")
print("-----------")
print(" 4 | 5 | 6 ")
print("-----------")
print(" 7 | 8 | 9 ")
print("\n\nThe legal keys with positions are like above board, so please enter positions carefully \n\n")

player1 = input(" Please enter your name : \n\n")
player2 = " Computer "
print(" " + player1 + " vs " + player2 + " ")
Board = {1:" ", 2:" ", 3:" ", 4:" ", 5:" ", 6:" ", 7:" ", 8:" ", 9:" "}
used_positions = [0]
is_playing = True


def win_conditions(move):
    return ((Board[1] == move and Board[2] == move and Board[3] ==  move) or
    (Board[4] == move and Board[5] == move and Board[6] ==  move) or
    (Board[7] == move and Board[8] == move and Board[9] ==  move) or
    (Board[1] == move and Board[4] == move and Board[7] ==  move) or
    (Board[2] == move and Board[5] == move and Board[7] ==  move) or
    (Board[3] == move and Board[6] == move and Board[9] ==  move) or
    (Board[1] == move and Board[5] == move and Board[9] ==  move) or
    (Board[3] == move and Board[5] == move and Board[7] ==  move))

#Check who is winner or not
def check_winner(move, player):
    if(win_conditions(move) == True):
        print_board()
        if player == player1:
            print("Hoooooray! You won the game.")
        else:
            print("Ooooops! Computer is winner! ")
        exit()
    else:
        pass

def player_move():
    try:
        move = int(input("Enter the key between 1 and 9"))
        if move not in used_positions and move < 10:
            Board.update({move:"X"})
            used_positions.append(move)
            #print(used_positions)
            check_winner('X', player1)
        else:
            print("Enter legal key for move")
            player_move()
    except Exception as e:
        player_move()


def computer_move():
    num = random.randint(1, 9)
    if num not in used_positions:
        Board.update({num:"O"})
        used_positions.append(num)
        #print(used_positions)
        check_winner('O', player2)
    else:
        computer_move() #continue


def print_board():
    print( " " + Board[1] + " | " + Board[2] + " | " + Board[3])
    print("---------")
    print( " " + Board[4] + " | " + Board[5] + " | " + Board[6])
    print("---------")
    print( " " + Board[7] + " | " + Board[8] + " | " + Board[9] + "\n")

print_board()

def main():
    total_move = 0
    while is_playing:
        if total_move < 10:
            player_move()
            check_winner('X', player1)
            computer_move()
            check_winner('O', player2)
            print_board()
            total_move = total_move + 2
        else:
            print("Game is tie")
            exit()

if __name__ == "__main__":
    main()
